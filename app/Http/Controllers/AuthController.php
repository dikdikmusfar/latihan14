<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){  
        return view('form');
    }

    public function home_post(Request $request){
        //dd($request->all());
        $nama = $request["nama"];
         return view('welcome',['nama' => $nama]);
     }
}
